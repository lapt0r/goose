FROM golang:1.16.4-alpine AS build
#build source directories
RUN mkdir /app
ADD . /app
WORKDIR /app
# pull in Go modules and build
RUN go mod download; go build ./cmd/goose
# Our start command which kicks off
# our newly created binary executable
ENTRYPOINT ["/app/goose"]
