module gitlab.com/lapt0r/goose

go 1.16

require (
	github.com/Microsoft/go-winio v0.5.0 // indirect
	github.com/abadojack/whatlanggo v1.0.1
	github.com/gogs/chardet v0.0.0-20191104214054-4b6791f73a28
	github.com/kevinburke/ssh_config v1.1.0 // indirect
	github.com/pelletier/go-toml v1.9.3 // indirect
	github.com/sergi/go-diff v1.2.0 // indirect
	github.com/sirupsen/logrus v1.8.1 // indirect
	github.com/texttheater/golang-levenshtein/levenshtein v0.0.0-20200805054039-cae8b0eaed6c
	github.com/xanzy/ssh-agent v0.3.0 // indirect
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.23.0
	golang.org/x/crypto v0.0.0-20210513164829-c07d793c2f9a // indirect
	golang.org/x/net v0.0.0-20210614182718-04defd469f4e // indirect
	golang.org/x/sys v0.0.0-20210611083646-a4fc73990273 // indirect
	gopkg.in/src-d/go-git.v4 v4.13.1
	gopkg.in/vmarkovtsev/go-lcss.v1 v1.0.0-20181020221121-dfc501d07ea0
)
