package main

import (
	"os"

	"gitlab.com/lapt0r/goose/cli"
)

func main() {
	os.Exit(cli.RunCLI(os.Args[1:]))
}
