package languagefilter

import (
	"fmt"
	"regexp"

	"github.com/abadojack/whatlanggo"
)

//IsTranslationFile does a language check to see if the finding may be a translation file.
func IsTranslationFile(path string, finding string) (bool, error) {
	languageCode := whatlanggo.DetectLang(finding).Iso6391()
	pattern := fmt.Sprintf("[/\\_.-]%s[/\\_.-]", languageCode)
	return regexp.MatchString(pattern, path)
}
