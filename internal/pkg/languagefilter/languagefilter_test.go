package languagefilter

import (
	"testing"
)

func TestLanguageFile(t *testing.T) {
	match := `es:
	time:
	  formats:
		date: "%m/%d/%y"
	simple_form:
	  labels:
		user:
		  firstname: Nombre
		  lastname: Apellido
		  user_type: "¿Qué es lo que mejor te describe?"
	helpers:
	  page_title:
		bibme:
		  prefix: 'BibMe:'
		  homepage: Generedor Gratuito de Citas & Bibliografía
		  with_style:
			without_item_type: Gratis %{style} Generador de Citas y Bibliografía
			with_item_type: Generar %{style} %{item_type} citas para tu bibliografóa
		  without_style: Generador Gratuito de Citas & Bibliografía - MLA, APA, Chicago, Harvard
		citation_machine:
		  prefix: Citation Machine
		  homepage: Generar formato &citas - APA, MLA, & Chicago
		  without_item_type: Fácil de usar
		  style: "%{style} generador de formato de citas"
		  with_item_type: para %{item_type}
	  page_meta_description:
		bibme:
		  prefix: 'BibMe:'
		  homepage: citas generadas rápidamente en APA, MLA, Chicago, Harvard y otros miles de estilos para tu  bibliografía. Es preciso y gratis!
		  with_style:
			without_item_type: citas y bibliografías generadas %{style} rápidamente. Es preciso y gratis!
			with_item_type: te permite fácil y automáticamente crear %{item_type} citas y hacer tu bibliografía en %{style}. Es preciso y gratis!
		  without_style: citas generadas rápidamente en APA, MLA, Chicago, Harvard y otros miles de estilos para tu  bibliografía. Es preciso y gratis!
		citation_machine:
		  prefix: Citation Machine® ayuda a estudiantes y profesionales a dar crédito adecuadamente a la información que utilizan.
		  homepage: Citar fuentes en APA, MLA, Chicago, Turabian, y Harvard gratis.
		  with_item_type: Cita tu  %{item_type}
		  without_item_type: Generar citas fácilmente.
		  style: en %{style} formato gratis
		  `
	file := "/target/config/locales/es.yml"
	isMatch, err := IsTranslationFile(file, match)
	if err != nil {
		t.Errorf("Error running test: %s", err.Error())
	}
	if !isMatch {
		t.Errorf("Expected %s to be a translation result.", match)
	}
}
