package configuration

import (
	"encoding/json"
	"fmt"
	"io/ioutil"

	_ "embed"
)

//go:embed goose_rules.json
var defaultConfig []byte

//ScanRule contains a friendly name, regex rule, confidence, and severity
type ScanRule struct {
	Name       string
	Rule       string
	Confidence float64
	Severity   string
}

//LoadConfiguration loads a set of ScanRules from a provided path target
func LoadConfiguration(path string) ([]ScanRule, error) {
	var contents []byte
	var err error
	if path != "" {
		contents, err = ioutil.ReadFile(path)

	} else {
		contents = defaultConfig
	}
	if err != nil {
		return nil, err
	}
	return unmarshalConfiguration(contents), err
}

func unmarshalConfiguration(b []byte) []ScanRule {
	var result []ScanRule
	err := json.Unmarshal(b, &result)
	if err != nil {
		fmt.Println("Error loading configuration: ", err)
	}
	return result
}
