package assignment

import (
	"fmt"
	"math"
	"regexp"
	"strings"

	"gitlab.com/lapt0r/goose/internal/pkg/entropy"
	"gitlab.com/lapt0r/goose/internal/pkg/reflectorfilter"
)

//Assignment is an externally visible struct to aid in deconstructing complex assignment statements into their conceptual base form
type Assignment struct {
	Type      string
	Name      string
	Separator string
	Value     string
}

//global regexes
var cmdLineRegex = regexp.MustCompile("(?i)^--?pa?s?s?wo?r?d")
var closingparenregex = regexp.MustCompile(`\);?$`)
var envVarRegex = regexp.MustCompile(`(\$\w+|%\w+%|\$[{(]\w+[})])`)
var secretregex = regexp.MustCompile(`(?i)(secret($|_|[()]|key$)|password$|(aes|ap[ip]|set)\S*(key?|token)s?$|connection[a-z0-9\-_]*string|webhook)`)
var invalidvalue = regexp.MustCompile("(?i)(string|scalar|true|false|\\[\\]|\\{\\}|''|\"\"|[01];?$|\\[\\d+\\]|test|null\\)?;?$|get|post)")
var urlcredregex = regexp.MustCompile("(?i)\\w+@[a-z0-9\\-]+\\.[a-z]{2,5}($|[\"']|/)")
var mailto = regexp.MustCompile("mailto")
var dictliteral = regexp.MustCompile("{[^{}]+}")
var invalidTypeRegex = regexp.MustCompile("(?i)(class|interface|func?)")
var invalidAssignmentRegex = regexp.MustCompile("(?i)(function|argv)")

//IsCommandLineArg returns whether or not the Assignment is a command line argument
func (assignment *Assignment) IsCommandLineArg() bool {
	return cmdLineRegex.MatchString(assignment.Name) && !envVarRegex.MatchString(assignment.Value)
}

//kb todo: specific cryptographic key detection

//IsHighEntropy returns whether or not the assignment value is considered high entropy
func (assignment *Assignment) IsHighEntropy() bool {
	return isHighEntropy(assignment.Value)
}

func isHighEntropy(str string) bool {
	entropyThreshold := 2.5
	metricEntropyThreshold := 0.5
	entropy := entropy.GetShannonEntropy(str)
	floatLen := float64(len(str))
	entropyMaxValue := math.Log2(floatLen)
	metricEntropy := (entropy / entropyMaxValue)
	return entropy > entropyThreshold && metricEntropy > metricEntropyThreshold
}

func (assignment *Assignment) HasHighEntropyArgument() bool {
	assignments := strings.Split(assignment.Value, ",")
	for _, a := range assignments {
		if isHighEntropy(a) {
			return true
		} else {
			continue
		}
	}
	return false
}

//HasWhitespace returns whether or not the assignment value contains whitespace
func (assignment *Assignment) HasWhitespace() bool {
	return regexp.MustCompile(`[^,]\s`).MatchString(assignment.Value)
}

//IsSecret returns whether or not the Assignment is considered to be a secret assignment
func (assignment *Assignment) IsSecret() bool {
	return assignment.IsValidValue() && secretregex.MatchString(assignment.Name) && assignment.IsKnownSecretAssignmentType()
}

//IsConfigAssignment returns whether or not the assignment matches a configuration value
func (assignment *Assignment) IsConfigAssignment() bool {
	return assignment.Type == "" && assignment.Name != "" && assignment.Separator != "" && (assignment.IsStringLiteral() || assignment.IsUnquotedString())
}

//IsKnownSecretAssignmentType returns whether or not the assignment is a known secret assignment type
func (assignment *Assignment) IsKnownSecretAssignmentType() bool {
	//if type wasn't set, dont' block on this
	if assignment.Type == "" {
		return true
	} else {
		return assignment.IsArrayAssignment() || assignment.IsDictAssignment() || assignment.IsStringLiteral() || assignment.IsConfigAssignment()
	}
}

//IsURLCredential returns whether or not the assignment matches a url credential
func (assignment *Assignment) IsURLCredential() bool {
	//thought: this is either a config value (likely unquoted) or a hardcoded string
	valueIsURLCredential := urlcredregex.MatchString(assignment.Value) && !mailto.MatchString(assignment.Value)
	return assignment.Separator == ":" && valueIsURLCredential
}

func (assignment *Assignment) IsMethodCall() bool {
	valueHasClosingParen := closingparenregex.MatchString(assignment.Value)
	return assignment.Separator == "(" && valueHasClosingParen
}

//IsStringLiteral checks if the assignment is a string literal
func (assignment *Assignment) IsStringLiteral() bool {
	stringliteral, _ := regexp.Compile("[\"'][^\"']+[\"']")
	return stringliteral.MatchString(assignment.Value)
}

//IsUnquotedString checks if the assignment is an unquoted string
func (assignment *Assignment) IsUnquotedString() bool {
	unquotedstring, _ := regexp.Compile(`\S+`)
	return unquotedstring.MatchString(assignment.Value)
}

//IsArrayAssignment checks if the assignment value is an array
func (assignment *Assignment) IsArrayAssignment() bool {
	arrayliteral, _ := regexp.Compile(`\[[^\]\[]+\]`)
	return arrayliteral.MatchString(assignment.Value)
}

//IsDictAssignment checks if the assignment is a dictionary
func (assignment *Assignment) IsDictAssignment() bool {

	return dictliteral.MatchString(assignment.Value)
}

//IsValidValue returns whether or not the value is invalid (true, false, or empty object/string)
func (assignment *Assignment) IsValidValue() bool {
	//valid secrets are 6 or more characters in 2020 systems.  Less than that is trivially brute-forceable.
	return !invalidvalue.MatchString(assignment.Value) && len(assignment.Value) > 5
}

//IsInvalidType returns whether or not the assignment has an invalid type
func (assignment *Assignment) IsInvalidType() bool {

	return invalidTypeRegex.MatchString(assignment.Name)
}

//IsInvalidAssignment returns whether or not the secret has an invalid assignment target (such as a function declaration or a password field value)
func (assignment *Assignment) IsInvalidAssignment() bool {
	return invalidAssignmentRegex.MatchString(assignment.Value)
}

//IsReflected returns whether or not the assignment is reflected
func (assignment *Assignment) IsReflected() bool {
	return reflectorfilter.IsReflected(fmt.Sprintf("%v = %v", assignment.Name, assignment.Value))
}

//IsLowEditDistance returns whether or not the assignment is a low edit distance from the value
func (assignment *Assignment) IsLowEditDistance() bool {
	return reflectorfilter.LowEditDistance(assignment.Name, assignment.Value)
}
