package loader

import (
	"io/ioutil"
	"testing"
)

func TestGitRepositoryLoadInvalidRepository(t *testing.T) {
	directory, err := ioutil.TempDir("", "InvalidGitRepo")
	if err != nil {
		t.Error(err)
	}
	targets, enumErr := EnumerateRepositoryFileChanges(directory, 10)
	if enumErr == nil {
		t.Errorf("Expected enumerating an invalid repository to fail")
	}
	if targets != nil {
		t.Errorf("Enumerating an invalid repository returned non-nil results")
	}
}
