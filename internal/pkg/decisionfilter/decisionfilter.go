package decisionfilter

import (
	"bufio"
	"log"
	"regexp"
	"strings"
	"sync"

	"gitlab.com/lapt0r/goose/internal/pkg/assignment"
	"gitlab.com/lapt0r/goose/internal/pkg/decisiontree"
	"gitlab.com/lapt0r/goose/internal/pkg/entropy"
	"gitlab.com/lapt0r/goose/internal/pkg/finding"
	"gitlab.com/lapt0r/goose/internal/pkg/loader"
)

var addTagRegexp = regexp.MustCompile("add")
var assignmentRegex = regexp.MustCompile("(<-|:=|:|=|\\()")
var cmdLineRegex = regexp.MustCompile("\\s--?")

//ScanFile scans a provided target with the decision tree scan engine
func ScanFile(target loader.ScanTarget, fchannel chan []finding.Finding, waitgroup *sync.WaitGroup) {
	defer func() {
		if r := recover(); r != nil {
			println("panic:" + r.(string))
		}
	}()
	defer waitgroup.Done()
	var findings []finding.Finding
	var secretRegexp = regexp.MustCompile("(?i)(password|token|secret|a(pi|ccess).?key|hook)")
	input, err := loader.GetBytesFromScanTarget(target)
	if err != nil {
		log.Fatal(err)
	}
	scanner := bufio.NewScanner(strings.NewReader(string(input)))
	index := 1 //Lines of code start at 1
	for scanner.Scan() {
		text := scanner.Text()
		if entropy.GetMetricEntropy(text) > 0.8 && secretRegexp.MatchString(text) {
			f := evaluateRule(strings.TrimSpace(text))
			if !f.IsEmpty() {
				f.Location = finding.Location{Path: target.Path, Line: index}
				findings = append(findings, f)
			}
		}
		index++
	}
	fchannel <- findings
}

func evaluateRule(input string) finding.Finding {
	var assignments = generateAssignments(input)
	if len(assignments) > 0 {
		if containsXMLTag(assignments[0].Name) && addTagRegexp.MatchString(assignments[0].Name) {
			// this is almost certainly an XML tag
			for _, f := range assignments {
				if regexp.MustCompile("(?i)(password|secret|token)").MatchString(f.Value) {
					return finding.Finding{
						Match:      input,
						Location:   finding.Location{},
						Rule:       "DecisionTree",
						Confidence: 0.7,    //todo: decision tree rules?
						Severity:   "high"} //todo: decision tree sets severity?
				}
			}
		} else {
			var filtered = filterAssignments(assignments)
			if len(filtered) > 0 {
				return finding.Finding{
					Match:      input,
					Location:   finding.Location{},
					Rule:       "DecisionTree",
					Confidence: 0.7,    //todo: decision tree rules?
					Severity:   "high"} //todo: decision tree sets severity?
			}
		}
	}
	return finding.Finding{}
}

func tokenize(input string) []string {
	return tokenizeWithSeparator(input, " ")
}

func tokenizeWithSeparator(input string, separator string) []string {
	return strings.Split(input, separator)
}

func filterAssignments(slice []assignment.Assignment) []assignment.Assignment {
	result := slice[:0]
	for _, x := range slice {
		//this is an odd check that should pass early
		if x.IsURLCredential() {
			result = append(result, x)
		} else if x.IsMethodCall() && !x.HasHighEntropyArgument() {
			//method calls MUST be high entropy otherwise they're probably junk
			continue
		} else if x.IsInvalidAssignment() || x.IsInvalidType() || !x.IsSecret() || x.IsReflected() || x.IsLowEditDistance() {
			continue
		} else {
			result = append(result, x)
		}
	}
	return result
}

//slick: https://github.com/golang/go/wiki/SliceTricks#filtering-without-allocating
func filterXMLTags(tokens []string) []string {
	result := tokens[:0]
	for _, x := range tokens {
		if !containsXMLTag(x) {
			result = append(result, x)
		}
	}
	return result
}

func containsXMLTag(token string) bool {
	var regex, err = regexp.Compile("[<>]")
	if err != nil {
		panic(err)
	}
	return regex.MatchString(token)
}

func generateAssignments(input string) []assignment.Assignment {
	//todo: some kind of normalization
	var results = make([]assignment.Assignment, 0)
	//command lines have their own grammar
	if cmdLineRegex.MatchString(input) {
		var tokens = tokenize(input)
		var tree = decisiontree.ConstructTree(tokens)
		results = generateAssignmentsRecursive(tree, " ")
	} else {
		var assignmentTokens = assignmentRegex.FindAllString(input, -1)
		for _, token := range assignmentTokens {
			separator := strings.TrimSpace(token)
			var tokens = tokenizeWithSeparator(input, separator)
			var tree = decisiontree.ConstructTree(tokens)
			results = append(results, generateAssignmentsRecursive(tree, separator)...)
		}
	}
	return results
}

func generateAssignmentsRecursive(node *decisiontree.Node, separator string) []assignment.Assignment {
	//oh boy here we go
	var result = make([]assignment.Assignment, 1)
	var item = &result[0]
	var current = node
	item.Separator = separator
	for {
		if current.IsCmdLineArgument() {
			item.Name = strings.TrimSpace(current.Value)
			if current.HasNext() {
				item.Value = strings.TrimSpace(current.Next.Value)
				result = append(result, generateAssignmentsRecursive(current.Next, separator)...)
			}
			break
		} else if item.Name == "" {
			item.Name = strings.TrimSpace(current.Value)
		} else {
			item.Value = strings.TrimSpace(current.Value)
			if current.HasNext() {
				result = append(result, generateAssignmentsRecursive(current.Next, separator)...)
				//recursive call will catch downstream stuff, break
				break
			}
		}
		if !current.HasNext() {
			break
		} else {
			//last = current
			current = current.Next
		}
	}
	return result
}
