package decisionfilter

import (
	"testing"
)

func testFalsePositives(cases []string, t *testing.T) {
	for _, teststring := range cases {
		result := evaluateRule(teststring)
		if !result.IsEmpty() {
			t.Errorf("expected no results for %v", teststring)
		} else {
			t.Logf("Test passed for %v", teststring)
		}
	}
}

func TestTokenize(t *testing.T) {
	var testString = "a b c"
	var result = tokenize(testString)

	if len(result) != 3 {
		t.Errorf("Expected 3 items but found %v", len(result))
	}

	if result[0] != "a" {
		t.Errorf("Expected first element to be 'a'  but was '%v'", result[0])
	}
	if result[1] != "b" {
		t.Errorf("Expected second element to be 'b'  but was '%v'", result[0])
	}
	if result[2] != "c" {
		t.Errorf("Expected third element to be 'c'  but was '%v'", result[0])
	}
}

func TestTokenizeWithSeparator(t *testing.T) {
	var testString = "a;b;c"
	var result = tokenizeWithSeparator(testString, ";")

	if len(result) != 3 {
		t.Errorf("Expected 3 items but found %v", len(result))
	}

	if result[0] != "a" {
		t.Errorf("Expected first element to be 'a'  but was '%v'", result[0])
	}
	if result[1] != "b" {
		t.Errorf("Expected second element to be 'b'  but was '%v'", result[0])
	}
	if result[2] != "c" {
		t.Errorf("Expected third element to be 'c'  but was '%v'", result[0])
	}
}

func TestContainsXMLTagHasTag(t *testing.T) {
	var testString = "<xml/>"
	var result = containsXMLTag(testString)
	if !result {
		t.Errorf("expected [%v] to contain an xml tag.", testString)
	}
}

func TestContainsXMLTagNoTag(t *testing.T) {
	var testString = "blah blah something xml blah/!@#$%"
	var result = containsXMLTag(testString)
	if result {
		t.Errorf("expected [%v] not to contain an xml tag.", testString)
	}
}

func TestXMLFilter(t *testing.T) {
	var testString = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>"
	var result = filterXMLTags(tokenize(testString))
	if len(result) != 2 {
		t.Errorf("Expected 2 results but found %v", len(result))
	}
	if result[0] != "version=\"1.0\"" {
		t.Errorf("Expected first result to be [version=\"1.0\"]  but was [%v]", result[0])
	}
	if result[1] != "encoding=\"utf-8\"" {
		t.Errorf("Expected first result to be [encoding=\"utf-8\"]  but was [%v]", result[1])
	}
}

func TestGenerateAssignmentsDoubleQuote(t *testing.T) {
	var testString = "foo = \"bar\""
	var result = generateAssignments(testString)
	if len(result) != 1 {
		t.Errorf("Expected 1 result in collection but got %v", len(result))
	}
	var assignment = result[0]
	if assignment.Name != "foo" {
		t.Errorf("expected name [foo] but found [%v]", assignment.Name)
	}
	if assignment.Value != "\"bar\"" {
		t.Errorf("expected value [\"bar\"] but found [%v]", assignment.Value)
	}
}

func TestGenerateAssignmentsSingleQuote(t *testing.T) {
	var testString = "foo = 'bar'"
	var result = generateAssignments(testString)
	if len(result) != 1 {
		t.Errorf("Expected 1 result in collection but got %v", len(result))
	}
	var assignment = result[0]
	if assignment.Name != "foo" {
		t.Errorf("expected name [foo] but found [%v]", assignment.Name)
	}
	if assignment.Value != "'bar'" {
		t.Errorf("expected value ['bar'] but found [%v]", assignment.Value)
	}
}

func TestGenerateAssignmentsRecursiveNoQuote(t *testing.T) {
	var testString = "foo = bar"
	var result = generateAssignments(testString)
	if len(result) != 1 {
		t.Errorf("Expected 1 result in collection but got %v", len(result))
	}
	var assignment = result[0]
	if assignment.Name != "foo" {
		t.Errorf("expected name [foo] but found [%v]", assignment.Name)
	}
	if assignment.Value != "bar" {
		t.Errorf("expected value [bar] but found [%v]", assignment.Value)
	}
}

func TestGenerateAssignmentsMultiAssignment(t *testing.T) {
	//kb note: parser currently throws up on extremely mixed assignments (:= and : or =)
	var testString = "foo = bar biz : baz"
	var result = generateAssignments(testString)
	if len(result) != 2 {
		t.Errorf("Expected 2 results in collection but got %v", len(result))
	}
	var assignment1 = result[0]
	if assignment1.Name != "foo" {
		t.Errorf("expected name [foo] but found [%v]", assignment1.Name)
	}
	if assignment1.Value != "bar biz : baz" {
		t.Errorf("expected value [bar biz : baz] but found [%v]", assignment1.Value)
	}
	var assignment2 = result[1]
	if assignment2.Name != "foo = bar biz" {
		t.Errorf("expected name [foo = bar biz] but found [%v]", assignment2.Name)
	}
	if assignment2.Value != "baz" {
		t.Errorf("expected value [baz] but found [%v]", assignment2.Value)
	}
}

func TestEvaluateRuleMatch(t *testing.T) {
	teststring := "password = deadbeef"
	result := evaluateRule(teststring)
	if result.IsEmpty() {
		t.Errorf("Expected 1 result but got an empty result")
	}
	if result.Match != teststring {
		t.Errorf("Expected match to be 'password' but was '%v'", result.Match)
	}
	if result.Confidence != 0.7 ||
		result.Rule != "DecisionTree" {
		t.Errorf("Expected confidence 0.5 and severity 1 but found confidence %v and severity %v", result.Confidence, result.Severity)
	}
}

func TestEvaluateRuleURLCredential(t *testing.T) {
	teststring := "admin:deadbeef@example.com"
	result := evaluateRule(teststring)
	if result.IsEmpty() {
		t.Errorf("expected 1 result but got an empty result")
	}
	if result.Match != teststring {
		t.Errorf("Expected match to be like admin:secret123 but was %v", result.Match)
	}
}

func TestEvaluateRuleMethodAssignmentEdgeCase(t *testing.T) {
	teststring := "setAmazonKeys ( 'AKIA1234567890ABCDEF' , 'deadbeef1234567890ABCDEF');"
	result := evaluateRule(teststring)
	if result.IsEmpty() {
		t.Errorf("expected 1 result but got an empty result")
	}
	if result.Match != teststring {
		t.Errorf("Expected match to be like %v but was %v", teststring, result.Match)
	}
}

func TestEvaluateRuleSecretNominal(t *testing.T) {
	teststrings := [...]string{"var client_secret = 'deadbeef'", "//api_token = \"DEADBEEFDEADBEEF\""}
	for _, teststring := range teststrings {
		result := evaluateRule(teststring)
		if result.IsEmpty() {
			t.Errorf("expected 1 result but got an empty result")
		}
		if result.Match != teststring {
			t.Errorf("Expected match to be like %v but was %v", teststring, result.Match)
		} else {
			t.Logf("test passed for %v", teststring)
		}
	}
}

func TestEvaluateRuleCompositeSecret(t *testing.T) {
	teststring := "secretkey = 'foobarbiz'"
	result := evaluateRule(teststring)
	if result.IsEmpty() {
		t.Errorf("expected 1 result but got an empty result")
	}
	if result.Match != teststring {
		t.Errorf("Expected match to be like %v but was %v", teststring, result.Match)
	}
}

func TestEvaluateRuleSecretMethodCall(t *testing.T) {
	//to investigate: how can we catch this in a more abstract fashion with the parser?
	teststring := "setsecret('AKIA1234567890ABCDEF')"
	result := evaluateRule(teststring)
	if result.IsEmpty() {
		t.Errorf("expected 1 result but got an empty result")
	}
	if result.Match != teststring {
		t.Errorf("Expected match to be like %v but was %v", teststring, result.Match)
	}
}

func TestEvaluateRuleFalsePositiveMapKeyAssignment(t *testing.T) {
	teststring := "key = key.map( jQuery.camelCase );"
	result := evaluateRule(teststring)
	if !result.IsEmpty() {
		t.Errorf("expected no results")
	}
}

func TestEvaluateRuleFalsePositiveIntegerSwitch(t *testing.T) {
	teststring := "int forcePasswordChange = 0;"
	result := evaluateRule(teststring)
	if !result.IsEmpty() {
		t.Errorf("expected no results")
	}
}

func TestEvaluateRuleFalsePositiveTestCode(t *testing.T) {
	teststring := "$password = 'testing';"
	result := evaluateRule(teststring)
	if !result.IsEmpty() {
		t.Errorf("expected no results")
	}
}

func TestEvaluateRuleFalsePositiveCryptoConfiguration(t *testing.T) {
	teststrings := []string{"nacl.box.secretKeyLength = crypto_box_SECRETKEYBYTES;"}
	testFalsePositives(teststrings, t)
}

func TestEvaluateRuleFalsePositiveArgumentValueIndexing(t *testing.T) {
	teststring := "$password = argv[0];"
	result := evaluateRule(teststring)
	if !result.IsEmpty() {
		t.Errorf("expected no results")
	}
}

func TestEvaluteRuleFalsePositiveJunkMatchPasswordValue(t *testing.T) {
	teststring := "string settingsTab = \"tab-password\";"
	result := evaluateRule(teststring)
	if !result.IsEmpty() {
		t.Errorf("expected no results")
	}
}

func TestEvaluateRuleFalsePositiveJunkMatchFunctionArgument(t *testing.T) {
	teststring := "ClearElement(By.Id(\"r-new-password\"), \"Clear r-new-password\");"
	result := evaluateRule(teststring)
	if !result.IsEmpty() {
		t.Errorf("expected no results")
	}
}

func TestEvaluateRuleFalsePositiveTypeDefinition(t *testing.T) {
	teststrings := []string{"reCaptchaSessionToken: string;", "paymentToken: string;", "password: string;", "webhooks: Scalars['Boolean'];"}
	testFalsePositives(teststrings, t)
}

func TestEvaluateRuleFalsePositiveAnonymousFunctionDeclaration(t *testing.T) {
	teststring := "Component.ResetPassword = function () {"
	result := evaluateRule(teststring)
	if !result.IsEmpty() {
		t.Errorf("expected no results")
	}
}

func TestEvaluateRuleFalsePositiveFunctionDefinitionPasswordNullDefault(t *testing.T) {
	teststring := "public function withUserInfo($user, $password = null);"
	result := evaluateRule(teststring)
	if !result.IsEmpty() {
		t.Errorf("expected no results")
	}
}

func TestEvaluateRuleFalsePositiveDotNetClassDefinition(t *testing.T) {
	teststring := "public class GetUserByEmailPassword : IReturn<UserResponse>"
	result := evaluateRule(teststring)
	if !result.IsEmpty() {
		t.Errorf("expected no results")
	}
}

func TestEvaluateRuleTruePositiveSlackWebhook(t *testing.T) {
	//this is a fake slack webhook stitched together from the the UUID 3134f556-6d15-4213-89e4-22abe0987ba9
	teststring := "private const string slackwebhook = \"https://hooks.slack.com/services/89e44213/22abe0987ba9/3134f5566d15\";"
	result := evaluateRule(teststring)
	if result.IsEmpty() {
		t.Errorf("expected 1 result")
	}
}

func TestEvaluateRuleFalsePositiveFunctionDeclaration(t *testing.T) {
	teststrings := []string{"usm.setThePassword(this._url, v);"}
	testFalsePositives(teststrings, t)
}

func TestEvaluateRuleFalsePositiveFunctionDefinitionImageBlob(t *testing.T) {
	teststrings := []string{"        'vYRjtYRjrXtjpXtjlGNje2tazoxazoRaxoxaxoRavYRatYRatX'."}
	testFalsePositives(teststrings, t)
}

func TestEvaluateRuleFalsePositiveReadRequestData(t *testing.T) {
	teststrings := []string{"$admin_password = isset($_POST['admin_password']) ? $_POST['admin_password'] : '';"}
	testFalsePositives(teststrings, t)
}

func TestEvaluateRuleFalsePositiveReflected(t *testing.T) {
	teststrings := []string{"private static final String PASSWORD = \"password\";"}
	for _, teststring := range teststrings {
		result := evaluateRule(teststring)
		if !result.IsEmpty() {
			t.Errorf("expected no results for %v", teststring)
		}
	}
}

func TestEvaluateRuleFalsePositiveJSConst(t *testing.T) {
	teststrings := []string{"const repeatPassword = body.repeat", "public passwordControl = new FormControl('', [Validators.required])"}
	testFalsePositives(teststrings, t)
}

func TestEvaluateRuleTruePositiveXMLNode(t *testing.T) {
	teststrings := [...]string{"[<add key=\"password\" value=\"deadbeef\"/>", "<add key=\"S3SecretKey\" value=\"DEADBEEF+DEADBEEF+DEADBEEF+DEADBEEF\"/>"}
	for _, teststring := range teststrings {
		result := evaluateRule(teststring)
		if result.IsEmpty() {
			t.Errorf("expected 1 results for %v", teststring)
		} else {
			t.Logf("Test passed for %v", teststring)
		}
	}
}

func TestEvaluateRuleTruePositiveCommandLineArgument(t *testing.T) {
	teststrings := []string{"<command>svn co --username test --password deadbeef --no-auth-cache http://www.somedomain.tld/some/path/to/resource</command>", "-username foo -password deadbeefdeadbeef -someflag", "            ApiSecret = \"deadbeefdeadbeef\",", "AESKey = \"deadbeefdeadbeefdeadbeef=\","}
	for _, teststring := range teststrings {
		result := evaluateRule(teststring)
		if result.IsEmpty() {
			t.Errorf("expected 1 results for %v", teststring)
		} else {
			t.Logf("Test passed for %v", teststring)
		}
	}
}

func TestEvaluateRuleFalsePositiveCommandLineArgument(t *testing.T) {
	teststrings := []string{"<command>svn co --username test --password %PASSWORD% --no-auth-cache http://www.somedomain.tld/some/path/to/resource</command>", "-username foo -password $PASSWORD -someflag", "-username foo -password ${PASSWORD} -someflag"}
	testFalsePositives(teststrings, t)
}

func TestEvaluateRuleFalsePositiveJavaErrorClass(t *testing.T) {
	teststrings := []string{"InvalidRefreshToken(6, INVALID_REFRESH_TOKEN),", "InvalidAccessToken(18, INVALID_ACCESS_TOKEN),", "fun getAccessToken(): String?"}
	testFalsePositives(teststrings, t)
}

func TestEvaluateRuleTruePositiveConfigurationValue(t *testing.T) {
	teststrings := []string{"FOOAPP_ACCESS_KEY=12345abcdef"}
	for _, teststring := range teststrings {
		result := evaluateRule(teststring)
		if result.IsEmpty() {
			t.Errorf("expected 1 results for %v", teststring)
		} else {
			t.Logf("Test passed for %v", teststring)
		}
	}
}
